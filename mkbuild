#!/bin/bash
#

log(){
	printf " :: $1 \n"
}

msg(){
	printf " => $1 \n"
}

err(){
	log "ERROR: $1"
	exit 1
}

trapi(){
	printf "\n"
	err "Trapping..."
}

print_help(){
	cat << EOF
Usage: `basename $0` [option]
Options:
	-a, --arch    change target
	-c, --clean   clean
	-h, --help    print this message
Support targets:
	`cd $TARGET_DIR; ls -m`
EOF
}

parser(){
	while [ "$1" ]; do
		case "$1" in
			-c|--clean)
				clean
				exit 0 ;;
			-a|--arch)
				arch=$2
				shift ;;
			-h|--help)
				print_help
				exit 0 ;;
		esac
		shift
	done
}

clean(){
	log "Clean"
	rm -rf $OUTPUT $BUILD_DIR
	sync
}

main(){
	set -e
	trap "trapi" INT ERR

	if [ -f "$CWD/config" ]; then
		. $CWD/config
	else
		err "Config file not found."
	fi
	
	[[ -d "$OUTPUT" ]] || install -dm755 $OUTPUT

	arch=${arch:-`uname -m`}

	parser "$@"

	if [ -f "${TARGET_DIR}/$arch" ]; then
		. "${TARGET_DIR}/$arch"
	else
		err "Unsupported '$arch' target."
		exit 1
	fi
	
	msg "CPU:    $CPU"
	msg "ARCH:   $ARCH"
	msg "CLIB:   $CLIB"
	msg "TARGET: $TARGET"
	printf "\n"

	for rec in $(ls $REC_DIR | sort) ; do
		rec_file=$(basename $rec)
		if [ -f "$OUTPUT/$rec_file" ]; then
			log "Found, skeep.."
		else
			log "Run: $rec_file"
			[[ -d "${BUILD_DIR}" ]] || install -dm755 ${BUILD_DIR}
			pushd ${BUILD_DIR} >/dev/null
				. $REC_DIR/$rec || err "'$rec' not found."
				(set -ex ; build)
			popd >/dev/null
			rm -rf ${BUILD_DIR}
			if [ "$?" = "0" ]; then
				touch $OUTPUT/$rec_file
			fi
		fi
	done
}

CWD="$PWD"
REC_DIR="$CWD/recepis"
SRC_DIR="${SRC_DIR:-$CWD/sources}"
BUILD_DIR="${BUILD_DIR:-$CWD/build_dir}"
TARGET_DIR="$CWD/targets"

main "$@"
